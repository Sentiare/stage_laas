# -*- coding: utf-8 -*-
#!/usr/bin/python

from __future__ import print_function
from gurobipy import *
from TGFReader import *
import sys
from math import *

Xmax = 6
liste = []
liste.append([])
liste[0].append("tgf/1/map_300_27670ppm.tgf")
liste[0].append("tgf/1/map_300_27781ppm.tgf")
liste[0].append("tgf/1/map_300_28026ppm.tgf")
liste[0].append("tgf/1/map_300_28584ppm.tgf")
liste[0].append("tgf/1/map_300_29163ppm.tgf")
liste[0].append("tgf/1/map_300_29186ppm.tgf")
liste[0].append("tgf/1/map_300_29409ppm.tgf")
liste[0].append("tgf/1/map_300_29721ppm.tgf")
liste[0].append("tgf/1/map_300_30055ppm.tgf")
liste[0].append("tgf/1/map_300_30523ppm.tgf")
liste.append([])
liste[1].append("tgf/1.5/map_300_59799ppm.tgf")
liste[1].append("tgf/1.5/map_300_61360ppm.tgf")
liste[1].append("tgf/1.5/map_300_61716ppm.tgf")
liste[1].append("tgf/1.5/map_300_62296ppm.tgf")
liste[1].append("tgf/1.5/map_300_62853ppm.tgf")
liste[1].append("tgf/1.5/map_300_63299ppm.tgf")
liste[1].append("tgf/1.5/map_300_63567ppm.tgf")
liste[1].append("tgf/1.5/map_300_63924ppm.tgf")
liste[1].append("tgf/1.5/map_300_65016ppm.tgf")
liste[1].append("tgf/1.5/map_300_66376ppm.tgf")
liste.append([])
liste[2].append("tgf/2/map_300_102452ppm.tgf")
liste[2].append("tgf/2/map_300_103879ppm.tgf")
liste[2].append("tgf/2/map_300_105039ppm.tgf")
liste[2].append("tgf/2/map_300_105797ppm.tgf")
liste[2].append("tgf/2/map_300_106577ppm.tgf")
liste[2].append("tgf/2/map_300_108584ppm.tgf")
liste[2].append("tgf/2/map_300_108851ppm.tgf")
liste[2].append("tgf/2/map_300_108940ppm.tgf")
liste[2].append("tgf/2/map_300_109810ppm.tgf")
liste[2].append("tgf/2/map_300_115406ppm.tgf")
liste.append([])
liste[3].append("tgf/2.5/map_300_149119ppm.tgf")
liste[3].append("tgf/2.5/map_300_150078ppm.tgf")
liste[3].append("tgf/2.5/map_300_152798ppm.tgf")
liste[3].append("tgf/2.5/map_300_157324ppm.tgf")
liste[3].append("tgf/2.5/map_300_157480ppm.tgf")
liste[3].append("tgf/2.5/map_300_157748ppm.tgf")
liste[3].append("tgf/2.5/map_300_159487ppm.tgf")
liste[3].append("tgf/2.5/map_300_161003ppm.tgf")
liste[3].append("tgf/2.5/map_300_161739ppm.tgf")
liste[3].append("tgf/2.5/map_300_162697ppm.tgf")
liste.append([])
liste[4].append("tgf/3/map_300_204838ppm.tgf")
liste[4].append("tgf/3/map_300_207335ppm.tgf")
liste[4].append("tgf/3/map_300_209052ppm.tgf")
liste[4].append("tgf/3/map_300_209587ppm.tgf")
liste[4].append("tgf/3/map_300_211482ppm.tgf")
liste[4].append("tgf/3/map_300_215228ppm.tgf")
liste[4].append("tgf/3/map_300_216142ppm.tgf")
liste[4].append("tgf/3/map_300_217279ppm.tgf")
liste[4].append("tgf/3/map_300_218795ppm.tgf")
liste[4].append("tgf/3/map_300_222943ppm.tgf")
liste.append([])
liste[5].append("tgf/3.5/map_300_272396ppm.tgf")
liste[5].append("tgf/3.5/map_300_274046ppm.tgf")
liste[5].append("tgf/3.5/map_300_274269ppm.tgf")
liste[5].append("tgf/3.5/map_300_274849ppm.tgf")
liste[5].append("tgf/3.5/map_300_279888ppm.tgf")
liste[5].append("tgf/3.5/map_300_280289ppm.tgf")
liste[5].append("tgf/3.5/map_300_284347ppm.tgf")
liste[5].append("tgf/3.5/map_300_288249ppm.tgf")
liste[5].append("tgf/3.5/map_300_289587ppm.tgf")
liste[5].append("tgf/3.5/map_300_294314ppm.tgf")


def solveNaif(name, X, N):
	# Retrieve Data
	x_ij, nbUser, nbNeighbors = readGraph(name)
	nbColor = N

	Val = 0
	Obj = 0
	T = 0

	try :
		# Create a new model
		m = Model("naif_17_06")

		# Create variables
		x_ic = m.addVars(nbUser, nbColor, vtype=GRB.BINARY)
		
		# Set objective
		m.setObjective(x_ic.sum(), GRB.MAXIMIZE)

		# Set constraints 
		for i in range(nbUser) :
			for j in range(nbUser) :
				if x_ij[i][j] == 1 :
					for k in range(nbColor) :                
						m.addConstr(x_ic[i,k] + x_ic[j,k] <= 1, "Neighbor Constraint : i{} j{} k{}".format(i,j,k))

		for i in range(nbUser) :
			m.addConstr(x_ic.sum(i, '*') <= 1, "Color Constraint : i{}".format(i))

		# Optimize :
		m.setParam('TimeLimit', 120)
		m.optimize()

		# Retrieve result :
		Val = float(1-m.MIPGap)
		Obj = m.objVal
		T = m.Runtime


	except GurobiError as e:
		print('Error code ' + str(e.errno) + ": " + str(e))

	except AttributeError as e:
		print('Encountered an attribute error' + str(e))

	return X, N, Val, Obj, T



#####################################################################################

argc = len(sys.argv)
if (argc <> 2) :
	print("Usage : python multiNaif.py <Max Color>")
	exit(-1)

with open("out.sol", "w") as f:
	for X in range(Xmax):
		for color in range(1,1+int(sys.argv[1])):
			Val=0.0
			for fileName in liste[X]:
				a, b, c, d,e = solveNaif(fileName, X, color)
				Val =float(Val + c)
			Val = Val / len(liste[X])
			f.write("(X={}, N={}) Validity={}\n".format(1+0.5*X, color, Val))
		 
		
	
	
		

