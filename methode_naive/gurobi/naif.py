# -*- coding: utf-8 -*-

from gurobipy import *
from TGFReader import *
import time
import copy

colorName = ["gray97","aquamarine", "bisque", "blue", "blueviolet", "brown", "cadetblue", "chartreuse", "chocolate", "cornflowerblue", "crimson", "darkgoldenrod1", "darkgreen", "aquamarine4", "chartreuse4"]


class Naif:
	def __init__(self):
		self.graph     = []
		self.name      = "empty"
		self.m         = None
		self.colors    = []
		self.score     = 0
		self.gap       = 1.0
		self.time      = 0

	def load(self, name):
		self.graph = readGraph(name)[0]
		self.name = name
		self.colors    = [0 for _ in range(len(self.graph))]

	def color(self, nbColor) :
		x_ij = self.graph
		nbUser = len(x_ij)
		try :
			# Create a new model
			m = Model("naif")

			# Create variables
			x_ic = m.addVars(nbUser, nbColor, vtype=GRB.BINARY)
			
			# Set objective
			m.setObjective(x_ic.sum(), GRB.MAXIMIZE)

			# Set constraints 
			for i in range(nbUser) :
				for j in range(nbUser) :
					if x_ij[i][j] == 1 :
						for k in range(nbColor) :                
							m.addConstr(x_ic[i,k] + x_ic[j,k] <= 1, "Neighbor Constraint : i{} j{} k{}".format(i,j,k))

			for i in range(nbUser) :
				m.addConstr(x_ic.sum(i, '*') <= 1, "Color Constraint : i{}".format(i))

			# Optimize :
			m.optimize()

		
			# Save color
			for i in range(nbUser) :
				for c in range(nbColor) :
					if (int(x_ic[i,c].x) == 1) :
						self.colors[i] = c+1

			self.gap = m.MIPGap
			self.score = m.objVal
			self.time = m.Runtime

		except GurobiError as e:
			print('Error code ' + str(e.errno) + ": " + str(e))

		except AttributeError:
			print('Encountered an attribute error')
	
	def run(self, n) :
		self.color(n)
		self.dot(self.name,True)
		self.sol(self.name)
		return 

	def usedColor(self) :
		return max(self.colors)
	 	
	def dot(self, name, activateColor) :
		# Name	
		name     = name.split("/")[-1].split(".",1)[0]	
		fullname = name+".dot"
		
		# Warning
		if (activateColor and (self.usedColor() > len(colorName))) :
			print("Warning: there is too many color in this graph to be color-printed.\n")
			activateColor = False

		# Write
		tab="    "
		with open(fullname, "w") as f:
			# Begin file
			f.write("graph "+name+" {\n")
			# Put the color
			if activateColor :
				for i in range(len(self.colors)) :
					if (self.colors[i] == 0) :
						f.write(tab+str(i)+";\n")
					else :
						f.write(tab+str(i)+"[style=filled, color="+colorName[self.colors[i]-1]+"];\n")
			# Put the edges
			for i in range(len(self.graph)) :
				for j in range(i+1,len(self.graph)) :
					if self.graph[i][j] == 1 :
						f.write(tab+str(i)+" -- "+str(j)+";\n")
			# End file
			f.write("}\n")
	
	def sol(self, name) :
		# Name
		name     = name.split("/")[-1].split(".",1)[0]	
		fullname = name+".sol"

		validity = float(1.0/(1.0+self.gap))

		# Write
		with open(fullname, "w") as f:
			# Begin file
			f.write("|["+fullname+"]|\n")
			f.write("Validity compared to Gurobi expectations : "+str(validity)+"\n")
			f.write("Coverage : " + str(self.score/float(len(self.graph)))+"\n")
			f.write("Duration : " + str(self.time)+"\n\n")
			f.write("|[colors]|\n")
			for c in range(len(self.colors)) :
				f.write(str(c)+" : "+str(self.colors[c])+"\n")


if __name__ == "__main__":
	argc = len(sys.argv)
	if argc <> 3 :
		print("Usage : python naif.py input.tgf nbColor")
		exit(-1)

	name = sys.argv[1]
	nbC  = int(sys.argv[2])

	naif = Naif()
	naif.load(name)
	naif.run(nbC)							
	
		
		
