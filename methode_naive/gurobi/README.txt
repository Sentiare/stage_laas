#######################################################
# APPROCHE NAIVE                                      #
#######################################################

Cette méthode est l'implémentation tel quel de notre 
problème. Elle renvoie un .sol et un .tgf qui décrivent
la solution trouvée. Arrête la recherche au bout de 2
minutes (il faudra que je le passes en argument).

#######################################################
# USAGE                                               #
#######################################################

python naif.py input.tgf nbColor :
	résout notre problème de coloration sur le
	graphe décrit par input.tgf avec nbColor 
	couleurs. Les solutions sont inscrites dans 
	un fichier .dot et un fichier .sol
