# -*- coding: utf-8 -*-
def readGraph(fileName) :
	with open(fileName, "r") as fileTGF :
		#// Collect Data //#
		content      = fileTGF.read()
		splitContent = content.split("#")
		vertices     = splitContent[0]
		edges        = splitContent[1]
		vertex       = vertices.split("\n")[:-1]
		edge         = [[x for x in y.split(" ")] for y in edges.split("\n")[1:-1]]

		#// Put Value Correctly //#
		out = [0]*len(vertex)
		for i in range(len(out)) :
			out[i] = [0]*len(vertex)

		for e in edge :
			out[int(e[0])][int(e[1])] = 1
			out[int(e[1])][int(e[0])] = 1

		#// Return Values //*
		return out, len(vertex), len(edge)


def printGraph(g) :
	for i in range(len(g)) :
		print(g[i])
		
				
		
