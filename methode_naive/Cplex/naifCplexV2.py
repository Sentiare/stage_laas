# -*- coding: utf-8 -*-
#!/usr/bin/python

from __future__ import print_function
from TGFReader import *
import sys

import cplex
from cplex.exceptions import CplexError

class Problem:
	def __init__(self):
		# Data
		self.name   = "none"
		self.matrix = []
		self.V      = 0
		self.E      = 0

		# Parameters
		self.nbColor = 0

		# Solver related
		self.prob    = None
		self.varName = []

		# Result
		self.score  = 0	
		self.colors = []


	def load(self, fileName):
		self.name = fileName.split("/")[-1].split(".")[0]
		self.matrix, self.V, self.E = readGraph(fileName)
		self.colors = [0 for _ in range(self.V)]

	def init(self, nbColor):
		self.nbColor = nbColor
		self._initVar()
		self._initConstr()
		self.prob.objective.set_sense(prob.objective.sense.maximize)

	def run(self):
		self.prob.solve()
		self.score = prob.solution.get_objective_value()
		self._colors()	
	
	def dot(self):
		#TBD

	def sol(self):
		#TBD

	def lp(self):
		self.prob.write(self.name+".lp")

	def _initVar(self):
		# Weights in obj :
		objList = []

		# Create var
		for c in range(self.nbColor):
			for u in range(self.V):
				objList.append(1.0)
				self.varName.append([u,c])

						
				
				

	def _initConstr(self):
		#TBD

	def _colors(self):
		#TBD
