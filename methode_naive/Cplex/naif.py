# -*- coding: utf-8 -*-
#!/usr/bin/python

from __future__ import print_function
from TGFReader import *
import sys

import cplex
from cplex.exceptions import CplexError

try :
	# Create a new model
	prob = cplex.Cplex()
	prob.set_problem_type(prob.problem_type.MILP)

	# Create variables
	varL = ["x01", "x02", "x11", "x12", "x21", "x22", "c1", "c2", "c3","j101", "j112", "j120", "j201", "j212", "j220"]
	objL = [1.0 if i < 6 else 0.0 for i in range(len(varL))] # Set objective weights
	lbL  = [0.0 for _ in varL]
	ubL  = [1.0 for _ in varL]
	typeL= [prob.variables.type.binary for _ in varL]

	# Set objective sense
	prob.objective.set_sense(prob.objective.sense.maximize)

	# Set constraints
	leC = []
	leC.append([["x01","x02","c1"]  ,[1.0,1.0,1.0]])
	leC.append([["x11","x12","c2"]  ,[1.0,1.0,1.0]])
	leC.append([["x21","x22","c3"]  ,[1.0,1.0,1.0]])
	leC.append([["x01","x11","j101"],[1.0,1.0,1.0]])
	leC.append([["x02","x12","j201"],[1.0,1.0,1.0]])
	leC.append([["x11","x21","j112"],[1.0,1.0,1.0]])
	leC.append([["x12","x22","j212"],[1.0,1.0,1.0]])
	leC.append([["x21","x01","j120"],[1.0,1.0,1.0]])
	leC.append([["x22","x02","j220"],[1.0,1.0,1.0]])
	rhsL = [1.0 for _ in leC]
	nameL = ["C{}".format(i+1) for i in range(3)]+["J{}".format(i+1) for i in range(6)]

	prob.variables.add(names=varL, obj=objL, lb=lbL, ub=ubL, types=typeL)
	prob.linear_constraints.add(lin_expr=leC, rhs=rhsL, names=nameL)	

	
	
	
	prob.solve()

except CplexError as e:
	print(e)
	exit(-1)

print()
# solution.get_status() returns an integer code
print("Solution status = ", prob.solution.get_status(), ":", end=' ')
# the following line prints the corresponding string
print(prob.solution.status[prob.solution.get_status()])
print("Solution value  = ", prob.solution.get_objective_value())

numcols = prob.variables.get_num()
numrows = prob.linear_constraints.get_num()

slack = prob.solution.get_linear_slacks()
x = prob.solution.get_values()

for j in range(numrows):
	print("Row %d:  Slack = %10f" % (j, slack[j]))
for j in range(numcols):
	print("Column %d:  Value = %10f" % (j, x[j]))

