#######################################################
# MAP                                                 #
#######################################################
Permet de générer un graph aléatoire en fixant ou non
certains paramètres. Les noeuds sont reliés les un aux
autres en fonction de leur proximité. Deux noeuds sont
considérés proches si la distance qui les sépare est 
inférieur à la distance d'interférence. La position des
noeuds est générée aléatoirement.

#######################################################
# USAGE                                               #
#######################################################

python map        : crée un graph 10x10 avec une
                    population inférieure à 200 et une
                    distance d'interférence de 1.
                    (format .tgf et .dot)
python map --help : aide pour l'utilisation 
                    détaillée
