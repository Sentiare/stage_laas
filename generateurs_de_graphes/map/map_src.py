# -*- coding: utf-8 -*-
import random
import os

def nearby(h1, h2, iD):
	vx = h2[1]-h1[1]
	vy = h2[2]-h1[2]	
	return ((vx*vx+vy*vy <= iD*iD)and((vx <> 0)and(vy <> 0)))

class map:
	def __init__(self):
		self.length = 0.0
		self.width  = 0.0
		self.population = []
		self.interferenceDistance = 0.0
		self.maxInterference = 0
		self.graph = []
	
	def setLength(self, l):
		self.length = l

	def setWidth(self, w):
		self.width = w
	
	def generatePopulation(self, nbHab):
		random.seed()
		self.population = []
		self.graph = []
		for i in range(nbHab):
			x = self.length*random.random()
			y = self.width*random.random()
			self.population.append([i,x,y])

		self.graph = [[0 for _ in range(nbHab)] for _ in range(nbHab)]

	
	def generateInterferences(self, iD):
		self.interferenceDistance = iD
		for h1 in self.population :
			inter = 0
			for h2 in self.population :
				if nearby(h1,h2,iD):
					self.graph[h1[0]][h2[0]] = 1
					self.graph[h2[0]][h1[0]] = 1
					inter+=1
			if (inter > self.maxInterference) :
				self.maxInterference = inter

	def clearInterferences(self):
		self.graph = [[0 for _ in range(nbHab)] for _ in range(nbHab)]

	def getGraph(self):
		return self.graph

	def getSurface(self):
		return float(self.length*self.width)

	def getDensity(self):
		return float(len(self.population)/(self.getSurface()))

	def getMaxInterference(self):
		return self.maxInterference

	def getInterferenceDensity(self):
		nbI  = float((sum([sum(i) for i in self.graph]))/2)
		pop = len(self.population)		
		maxI = float(pop*(pop-1)/2)
		return  float(int(1000000*(nbI/maxI)))

	def writeInFile(self, name):
		if not os.path.exists("tgf"):
			os.mkdir("tgf")
		with open("tgf/"+name, "w") as f :
			for i in range(len(self.population)) :
				f.write(str(i)+"\n")
			f.write("#\n")
			for i in range(len(self.population)) :
				for j in range(i, len(self.population)) :
					if self.graph[i][j] == 1 :
						f.write(str(i)+" "+str(j)+"\n")

	def dot(self, name):
		if not os.path.exists("dot"):
			os.mkdir("dot")
		with open("dot/"+name, "w") as f:
			f.write("graph unknown {\n")
			for i in range(len(self.population)) :
				if (sum(self.graph[i]) == 0):
					f.write(str(i)+";\n")
				else :
					for j in range(i, len(self.population)) :
						if self.graph[i][j] == 1 :
							f.write(str(i)+" -- "+str(j)+";\n")
			f.write("}\n")
	


	def generateName(self):
		return "map_"+str(len(self.population))+"_"+str(int(self.getInterferenceDensity()))+"ppm"


















































