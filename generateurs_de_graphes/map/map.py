# -*- coding: utf-8 -*-
from map_src import *
import sys
import random

argc = len(sys.argv)
random.seed()
usage = "Usage: python map.py [-l <length> || -w <width> || -p <population> || -i <interference distance>]"
#Par defaut :
l = 20.0
w = 20.0
p = random.randint(0,200)
iD = 1.0

i = 1	
while (i < argc):

	#Traitement des arguments
	if ((sys.argv[i]=="--help")or(sys.argv[i]=="-h")or(sys.argv[i]=="-H")):
		print(usage)
		exit(0)
	elif (sys.argv[i]=="-l"):
		if (argc>=i+1):
			i+=1		
			l = float(sys.argv[i])
		else:
			print(usage)
			exit(-1)
	elif (sys.argv[i]=="-w"): 
		if (argc>=i+1):
			i+=1
			w = float(sys.argv[i])
		else:
			print(usage)
			exit(-1)
	elif (sys.argv[i]=="-p"):
		if (argc >= i+1) :
			i+=1
			p = int(sys.argv[i])
		else:
			print(usage)
			exit(-1)
	elif (sys.argv[i]=="-i"):
		if (argc >= i+1) :
			i+=1
			iD = float(sys.argv[i])
		else:
			print(usage)
			exit(-1)
	else :
		print(usage)
		exit(-1)
	i+=1

#Vérifier la validité
if ((l<=0.0)or(w<=0.0)):
	print("Error: Dimensions must be strictly positive real values.")
	exit(-1)
if (iD<=0.0) :
	print("Error: Distances must be positive real values.")
	exit(-1)
if (p<=0) :
	print("Error: Population must have a natural values (but 0).")
	exit(-1)

#Créer la map :
m = map()
m.setLength(l)
m.setWidth(w)
m.generatePopulation(p)
m.generateInterferences(iD)

#Imprimer la map
s = m.generateName()+".tgf"
m.writeInFile(s)

#dot
s = m.generateName()+".dot"
m.dot(s)



