#######################################################
# GRAPH GENERATOR                                     #
#######################################################
Permet de générer un graph aléatoire en fixant ou non
certains paramètres. Aucune considération de proximité
n'est utilisée lors de la génération des arcs. Les
noeuds sont générés et liés aléatoirement

#######################################################
# MAKE FILE                                           #
#######################################################

make          : crée GraphGenerator
make clean    : supprime les fichiers de constructions
make cleaner  : + supprime GraphGenerator
make cleanest : + supprime tous les .tgf générer ici

#######################################################
# USAGE                                               #
#######################################################

./GraphGenerator        : crée un graph aléatoire au 
                          format .tgf
./GraphGenerator --help : aide pour l'utilisation 
                          détaillée
