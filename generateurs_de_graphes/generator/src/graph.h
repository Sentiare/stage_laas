#ifndef GRAPH_H
#define GRAPH_H


//# Structure ######//
struct edge {
	int i;
	int j;
	struct edge * next;
};
typedef struct edge S_edge;

typedef struct {
	S_edge * firstEdge;
	int ** edgeTable;
	int vertices;
	int edges;
	int tag;
} S_graph;

//# Functions ######//
S_graph * graph(int v, int e, int cc, int cs);
void info(S_graph * g);
void destroy(S_graph * g);

#endif
