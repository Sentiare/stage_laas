#include "printer.h"
#include "graph.h"
#include "alea.h"
#include <stdlib.h>
#include <stdio.h>

//# Global ####// 
char * extToString[2] = {"out", "tgf"}; 

void printGraph(FORMAT ext, S_graph * g, char* fileName) {
	if (ext == TGF) {
		_printGraphTGF(g, fileName);
	} else {
		printf("Error : the requested extension for '%s' is not supported\n", fileName);
		exit(EXIT_FAILURE);
	}
}

void _printGraphTGF(S_graph * g, char* fileName) {
	//Declaration :
	FILE * f = NULL;
	int i=0;
	S_edge * e = NULL;

	//Create the file :
	f = fopen(fileName, "w");
	if (f == NULL) {
		printf("Error : the requested file '%s' cannot be open in write mode\n", fileName);
		exit(EXIT_FAILURE);
	}

	//Write the first part "Vertices":
	for (i=0; i < g->vertices; i++) {
		fprintf(f, "%d\n", i);
	}
	
	//Write a #
	fprintf(f, "#\n");

	//Write the second part "Edges":
	e = g->firstEdge;
	while (e != NULL) {
		fprintf(f,"%d %d\n", e->i, e->j);
		e = e->next;
	}
}

char * name(S_graph * g, FORMAT ext) {
	//Gestion des paramètres
	if (g == NULL) {
		printf("Error Null Pointer : There's no graph to name.\n");
		return "null";
	}
	if (ext == TGF) {
	} else {
		printf("Warning : the requested extension is not supported\n");
		printf("default value will be used.\n");
		ext = ERR;
	}
	size_t needed = snprintf(NULL, 0,"generated/graph_%d_%d_%d.%s", g->vertices, g->edges, g->tag, extToString[ext]) +1;
	char * out = malloc(needed);
	sprintf(out, "generated/graph_%d_%d_%d.%s", g->vertices, g->edges, g->tag, extToString[ext]);
	return out;
}
