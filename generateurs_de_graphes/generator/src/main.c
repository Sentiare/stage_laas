#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "printer.h"
#include "graph.h"
#include "alea.h"


#define usage() printf("usage : GraphGenerator [-v <vertex> || -e <edge> || -c <count> <size> || --help]\n");printf("Use 'GraphGenerator --help' for further informations.\n");exit(EXIT_FAILURE)

#define fullUsage "GraphGenerator - gg:\n>>Parameters\n[-v <vertex>]       : set the number of vertices.\n[-e <edge>]         : set the number of edges.\n[-c <count> <size>] : set the count and size of required clique. Cliques of higher order may nonetheless randomly appear. Fulfilling this requirement may result in exceeding the expected number of edges.\n"


int main (int argc, char * argv[]) {
	//Declaration
	int cpt = 1;	
	int v = -1;
	int e = -1;
	int cs = 2;
	int cc = 0;
	struct stat st = {0};

	//Traitement des paramètre d'entrée
	while (cpt < argc) {
		if (0 == strcmp(argv[cpt], "-v")) {
			if (cpt+1 < argc) {
				v = atoi(argv[cpt+1]);
				cpt++;
			} else {
				usage();

			}
		} else if (0 == strcmp(argv[cpt], "-e")) {
			if (cpt+1 < argc) {
				e = atoi(argv[cpt+1]);
				cpt++;
			} else {
				usage();
			}
		} else if (0 == strcmp(argv[cpt], "-c")){
			if (cpt+2 < argc) {
				cc = atoi(argv[cpt+1]);
				cs = atoi(argv[cpt+2]);
				cpt+=2;	
			}
		} else if (0 == strcmp(argv[cpt], "--help")) {
			printf(fullUsage);
			exit(EXIT_SUCCESS);	
		} else {
			usage();			
		}
		cpt++;
	}	


	//truc utile
	initAlea();
	S_graph * g = graph(v, e, cc, cs);
	if (NULL != g) {
		if (stat("generated", &st) == -1) {
			mkdir("generated", 0740);		
		}
		printGraph(TGF, g, name(g, TGF));
		info(g);
		destroy(g);
	}

}


