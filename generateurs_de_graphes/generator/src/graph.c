#include "graph.h"
#include "alea.h"
#include <stdlib.h>
#include <stdio.h>

S_graph * graph(int v, int e, int cc, int cs) {
	//Gestion des arguments :
	if (v == -1) {
		v =randint(2, 1000);
	}
	if (v <= 1) {
		printf("Error : a graph must have at least two vertex\n");
		return NULL;
	}
	if ((e > (v*(v-1))/2)||(0 > e)) {
		e = randint(0, (v*(v-1))/2);
	}
	if (cc < 0) {
		printf("Warning : A negative amount of clique cannot be generated. No clique will willingly be generated instead.\n");
		cc = 0;
	}
	if (cs < 2) {
		printf("Warning : it requires 2 nodes to create a decent clique.\n");
		cs = 1;
	}
	if (v < cc+cs-1) {
		printf("Error : There is not enough vertices to create number of cliques of the required size.\n");
		return NULL;
	}

	//Déclaration :
	int cpt = 0;
	int kpt = 0;
	int qpt = 0;
	int ec = 0;
	int i = -1;
	int j = -1;
	int clique[cs];
	int in = 0;
	S_edge * save = NULL;

	//Malloc
	S_graph * out = (S_graph *)malloc(sizeof(S_graph));
	
	//Valeurs connues :
	out->vertices  = v;
	out->firstEdge = NULL;
	out->tag       = randint(0,100);
	
	//Création de la table des arretes :
	out->edgeTable = (int **)malloc(v*sizeof(int*));
	for(cpt = 0; cpt < v; cpt++) {
		(out->edgeTable)[cpt] = (int *)malloc(v*sizeof(int));
		for(kpt = 0; kpt < v; kpt++) {
			(out->edgeTable)[cpt][kpt] = 0;
		}
	}

	//Création de cliques
	for(cpt = 0; cpt < cc; cpt++) {
		for(kpt = 0; kpt < cs; kpt++) {
			in = 0;
			clique[kpt] = randint(0,v);
			for (qpt = 0; qpt < kpt; qpt++) {
				if(clique[kpt] == clique[qpt]) {
					in++;
				}
			}
			if(in != 0) {
				kpt--;
			}
		}

		for(kpt = 0; kpt < cs; kpt++) {
			for(qpt = kpt+1; qpt < cs; qpt++) {
				i = clique[kpt];
				j = clique[qpt];
				(out->edgeTable)[i][j] = 1;
				(out->edgeTable)[j][i] = 1;
				save = out->firstEdge;
				out->firstEdge = (S_edge *)malloc(sizeof(S_edge));
				out->firstEdge->i = i;
				out->firstEdge->j = j;
				out->firstEdge->next = save;
				e--;
				ec++;
			}
		}
	}

	//Génération de valeurs aléatoires :
	for(cpt = 0; cpt < e; cpt++) {
		i = randint(0, v);
		j = randint(0, v);
		if (i != j) {
			if ( (out->edgeTable)[i][j] == 0 ) {
				(out->edgeTable)[i][j] = 1;
				(out->edgeTable)[j][i] = 1;
				save = out->firstEdge;
				out->firstEdge = (S_edge *)malloc(sizeof(S_edge));
				out->firstEdge->i = i;
				out->firstEdge->j = j;
				out->firstEdge->next = save;
				ec++;
			} else {
				cpt--;
			}
		} else {
			cpt--;
		}
	}

	//Dernier ajout :
	out->edges = ec;

	//Return
	return out;		
}

void info(S_graph * g) {
	float v = -1;
	float e = -1;
	if(g!=NULL) {
		v = (float)g->vertices;
		e = (float)g->edges;
		
		printf("GRAPH INFO\n");
		printf(" | vertices      : %d\n", (int)v);		
		printf(" | edges         : %d\n", (int)e);
		printf(" | density ratio : %d%%\n",  (int)(100*(2*e)/(v*(v-1))) );
		printf(" | tag           : %d\n", g->tag);
		
	}
}

void destroy(S_graph * g) {
	//Gestion des paramètres
	if (g == NULL) {
		printf("Error Null Pointer : There's no graph to destroy.\n");
		exit(EXIT_FAILURE);
	}

	//Déclaration
	S_edge * e      = g->firstEdge;
	S_edge * next_e = NULL;	
	int i=0;

	//Free the edges :
	while (e != NULL) {
		next_e = e->next;
		free(e);
		e = next_e;
	}
	
	//Free the table :
	for (i=0; i<g->vertices; i++) {
		free((g->edgeTable)[i]);
	}
	free(g->edgeTable);

	//Free the structure :
	free(g);
}


