#ifndef PRINTER_H
#define PRINTER_H

//# INCLUDE ###//
#include "graph.h"

//# Enum ######//
typedef enum {
	ERR,
	TGF
} FORMAT;

//# Functions #//
void printGraph(FORMAT ext, S_graph * g, char* fileName);
void _printGraphTGF(S_graph * g, char* fileName);
char * name(S_graph * g, FORMAT ext);

#endif
