#######################################################
# HEURISTIQUE DSATUR                                  #
#######################################################
Adaptation de DSATUR afin de réaliser le limiter à K
couleur. On obtient une borne inférieure de notre
problème.

Utilise des fichier au format .tgf en entrée.

#######################################################
# USAGE                                               #
#######################################################

python DSATUR.py input.tgf output.sol colorLimit :
	Colore un maximum de noeuds du graphe décrit
	par input.tgf avec colorLimit couleurs. Le
	résultat est sauvegarder dans output.sol.
