from TGFReader import *
import time
import sys

class GraphDSATUR:
	def __init__(self,g):
		self._graph       = g
		self._vertices    = [] # [[indice, degree, saturation, couleur, couleurDispo]]
		self.initVertices()
	
	def initVertices(self):
		self._vertices = []
		for i in range(len(g)):
			self._vertices.append([i,sum(g[i]), 0, 0,[]])

	def order(self):
		self._vertices.sort(key=lambda x: (-x[2], -x[1]))
		


	def neighbors(self, index):
		return [v for v in range(len(self._graph[index])) if self._graph[index][v] == 1]
	
	def _neighborsK(self, index):
		n = self.neighbors(self._vertices[index][0])
		nk = []
		for ind in n :
			nk.append( next(i for i in range(len(self._vertices)) if self._vertices[i][0] == ind) )
		return nk

	def neighborsColors(self, index):
		nk = self._neighborsK(self, index)
		return [self._vertices[i][3] for i in nk]	

	def _getNext(self):
		return next(( (self._vertices[i][4][0],i) for i in range(len(self._vertices)) if (len(self._vertices[i][4])<>0) ), (None, None))

	def _color(self, index, color) :
		if (color in self._vertices[index][4]) :
			self._vertices[index][3] = color
			self._vertices[index][4] = []
			self._vertices[index][2] = 0
			for nk in self._neighborsK(index) :
				if (color in self._vertices[nk][4]) :		
					self._vertices[nk][2] = self._vertices[nk][2]+1
					self._vertices[nk][4].remove(color)
					
						

	def solve(self,k, inName, outName, e):
		t = time.time()
		coloration = [0]*(k+1)
		coloration[0] = len(self._graph)
		for i in range(len(self._vertices)) :
			self._vertices[i][4] = range(1,k+1)
					
		self.order()
		color, node = self._getNext()
		while (not(node is None)):
			self._color(node, color)
			self.order()
			coloration[0] = coloration[0]-1
			coloration[color] = coloration[color]+1
			color, node = self._getNext() 
		t = time.time() - t		

		with open(outName,"w") as f :
			strg = "# Solution for "+inName+" #"
			sep  = "#"*len(strg)
			f.write(sep+"\n")
			f.write(strg+"\n")
			f.write(sep+"\n")
			f.write("Colors    :"+str(k)+"\n")
			f.write("Vertices  :"+str(len(self._vertices))+"\n")
			f.write("Edges     :"+str(e)+"\n")
			f.write(sep+"\n")
			v = len(self._vertices)
			l = coloration[0]
			f.write("Coverage     : "+str(int(100*(float(v-l))/float(v)))+"%\n")
			f.write("Resolve time : "+str(t)+"s\n")
			f.write(sep+"\n")
			for col in range(k+1) :
				f.write(str(col)+' : '+str([n[0] for n in self._vertices if n[3] == col])+"\n")		
			f.write(sep+"\n")


if (len(sys.argv) != 4) :
	print("Usage : python DSATUR.py in.tgf out.sol color")
	exit(-1)

g, v, e= readGraph(sys.argv[1])
pb = GraphDSATUR(g)
pb.solve(int(sys.argv[3]), sys.argv[1], sys.argv[2], e)

